import express from 'express';
import favicon from 'serve-favicon';
import { isWorker } from 'cluster';
import { resolve } from 'dns';


var app = require('express')();
const path = require("path");
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var fs = require('fs');
var moment = require('moment');
var Apa102spi = require('apa102-spi');

// Sensoren initialiseren
const HIH6030 = require('./classes/HIH6030').default
const iAQCore = require('./classes/iAQCore').default
const VEML7700 = require('./classes/VEML7700').default

const PORT = 3000;

const LEDS = 17;

var ledsInDarkOn = 0;     // 0 = off    1 = on
var maxLight = 3000;
var lightOffset = 0;
var temperatureOffset = 10;
var co2Offset = 0;
var humidityOffset = 0;

var co2ArrayMean = [];
var tempArrayMean = [];
var humArrayMean = [];
const refreshTimeSaveInMin = 5;
const refresTimeWebsiteInSec = 1;

var startupInterval;
var startupIntervalTimer = 100;
var startup = true;
var startupState = true;
var startupLastLed = 0;
var startupTimer = false;
var startupColor = {
  "Red": 255,
  "Green": 128,
  "Blue": 0,
  "RedDir": 1,
  "GreenDir": 1,
  "BlueDir": -1
};

var errorInterval;
var errorIntervalTimer = 100;
var errorCounter = 0;
var errorMode = false;
var errorLedsOn = false;
var errorBrightness = 0;
var errorBrightnessDir = 1;

// This is for the public folder on path /
app.use(express.static(path.join(__dirname, 'public')));

app.use(favicon(path.join(__dirname, 'public', '/images/favicon.png')));


var LedDriver = new Apa102spi(LEDS, 100);

var refreshTimeout = setInterval(async() => {
  var data = await messureData();
  //update leds
  if(startup){
    if(startupTimer == false){
      startupInterval = setInterval(startupLeds, startupIntervalTimer);
      startupTimer = true;
    }
  } else {
    if(data != null) {
      var color = getLedColor(data.co2);
      var brightness = VEML7700.getLight() / maxLight * 31 - lightOffset;
      brightness = (brightness > 31) ? 31 : brightness;
      brightness = (brightness < ledsInDarkOn) ? ledsInDarkOn : brightness;
      for(var x = 0; x <= LEDS; x++) {
        if(x < color.amount)
          LedDriver.setLedColor(x, brightness, color.red, color.green, color.blue);
        else
          LedDriver.setLedColor(x, brightness, 0, 0, 0)
      }
      LedDriver.sendLeds();
    }
  }
  //update websites
  if(data != null) {
    //update websites
    io.emit('co2 messure', data.co2);
    io.emit('temp messure', data.temp);
    io.emit('hum messure', data.hum);
    io.emit('time messure', data.time);
    //add data to list for calculate the mean
    co2ArrayMean.push(data.co2);
    tempArrayMean.push(data.temp);
    humArrayMean.push(data.hum);
  }
},refresTimeWebsiteInSec * 1000);

var saveTimeout = setInterval(() => {
  // calculate the mean of the last 5 minutes
  var sum = 0;
  for(var value in co2ArrayMean) {
    sum += parseInt(co2ArrayMean[value]);
  }
  var co2Mean = Math.round(sum / co2ArrayMean.length)
  co2ArrayMean = [];
  sum = 0;

  for(var value in tempArrayMean) {
    sum += parseInt(tempArrayMean[value]);
  }
  var tempMean = Math.round(sum / tempArrayMean.length * 10) / 10
  tempArrayMean = [];
  sum = 0;

  for(var value in humArrayMean) {
    sum += parseInt(humArrayMean[value]);
  }
  var humMean = Math.round(sum / humArrayMean.length)
  humArrayMean = [];
  //save to file

  var coeff = 1000 * 60 * refreshTimeSaveInMin;
  var dateToRound = new Date();  //or use any other date
  var data = {
    'co2': co2Mean,
    'temp': tempMean,
    'hum': humMean,
    'time': new Date(Math.floor(dateToRound.getTime() / coeff) * coeff)
  }

  // writeToWeekFile(data);
  writeToLast7DaysFile(data);
}, refreshTimeSaveInMin * 60 * 1000);

function messureData() {
  return new Promise(resolve => {
    var data = null;
      // read new data
      iAQCore.read();
      HIH6030.write();
      VEML7700.startSensor();
      return setTimeout(() => {
        VEML7700.read();  //after reading, the sensor turns off.
        HIH6030.read();
        // check if data is correct
        if(HIH6030.getStatus() == 0 && iAQCore.getStatus() == 0)
        {
          errorCounter = 0;
          if(errorMode) {
            errorMode = false;
            clearInterval(errorInterval);
          }
          //startup mode ends the first time we messure correct vallues
          if(startup){
            startup = false;
            clearInterval(startupInterval);
            startupTimer = false;
          }

          var co2 = iAQCore.getCO2() - co2Offset;
          var temp = Math.round(HIH6030.getTemp() * 10) / 10 - temperatureOffset;
          var hum = Math.round(HIH6030.getHum()) - humidityOffset;
          var dateNow = new Date();
          var time = dateNow.toUTCString();
          data = {
            "co2": co2,
            "temp": temp,
            "hum": hum,
            "time": time
          }
        } else {
          errorCounter = (errorCounter >= 2) ? 2 : errorCounter++;
          if(errorCounter >= 2 && errorMode == false && startup == false){
            errorMode = true;
           errorInterval = setInterval(errorLeds, errorIntervalTimer);
          }
          if(HIH6030.getStatus() != 0){
            console.log(`${new Date().toUTCString()}; statusbit HIH6030 = ${HIH6030.getStatus()}`)
          }
          if(iAQCore.getStatus() != 0){            
            console.log(`${new Date().toUTCString()}; statusbit iAQCore = ${iAQCore.getStatus()}`)
          }
          data = null;
        };
        resolve(data);
      }, 200);
  });
}

// to update the charts on the website

function writeToLast7DaysFile(data) {
  var oldData = null;
  var dateNow = new Date(data.time);
  try {
    var rawData = fs.readFileSync(path.join(__dirname, `data/last7Days.json`));
  } catch(err) {
    rawData = "{}";
    console.log(`${new Date().toUTCString()}; error reading last 7 days file`);
  }
  oldData = JSON.parse(rawData);
  var newData = {};
  for(var element in oldData) {
    var day = new Date(element);
    // make a new array without objects older then a week
    if(dateNow.getTime() - day.getTime() <= 604800000) {
      newData[element] = oldData[element];
    }
    day = null;
  };
  newData[data.time] = {
    "co2": data.co2,
    "temp": data.temp,
    "hum": data.hum,
  }
  var dataAsString = JSON.stringify(newData);
  fs.writeFileSync(path.join(__dirname, `data/last7Days.json`), dataAsString);
}

function getLedColor(co2){
  var color = {
    "red": 0,
    "green": 0,
    "blue": 0,
    "amount": 0
  }
  
  color.red = parseInt((co2 ) / 1550 * 255);
  color.red = (color.red > 255) ? 255 : color.red;
  color.green = 255 - color.red;
  color.amount = parseInt((co2 - 450 ) / 1550 * LEDS + 2);
  return color;
}


function startupLeds(){
  var brightness = VEML7700.getLight() / maxLight * 31 - lightOffset;
  brightness = (brightness > 31) ? 31 : brightness;
  brightness = (brightness < ledsInDarkOn) ? ledsInDarkOn : brightness;

  updateStartupColor();

  for(var x = 0; x <= LEDS; x++){
    if(startupState){       // startupState says if leds going up or down
      if(x <= startupLastLed){
        LedDriver.setLedColor(x, brightness, startupColor.Red, startupColor.Green, startupColor.Blue);
      } else {
        LedDriver.setLedColor(x, brightness, 0, 0, 0);
      }
    } else {
      if(x + 1 >= startupLastLed){
        LedDriver.setLedColor(x, brightness, 0, 0, 0);
      } else {
        LedDriver.setLedColor(x, brightness, startupColor.Red, startupColor.Green, startupColor.Blue);
      }
    }
  }
  if(startupState){
    startupLastLed++
  } else {
    startupLastLed--
  }
  if(startupLastLed >= 16)
  {
    startupState = !startupState;
  }
  if(startupLastLed <= 0){
    startupState = !startupState;
  }
  LedDriver.sendLeds();
}

function updateStartupColor(){
  var val = 10;
  if(startupColor.RedDir == 1)
  {
    if(startupColor.Red + val >= 255){
      startupColor.RedDir *= -1;
      startupColor.Red -= val;
    }
    startupColor.Red += val;
  } else {
    if(startupColor.Red - val <= 0){
      startupColor.RedDir *= -1;
      startupColor.Red += val;
    }
    startupColor.Red -= val;
  }
  if(startupColor.GreenDir == 1)
  {
    if(startupColor.Green + val >= 255){
      startupColor.GreenDir *= -1;
      startupColor.Green -= val;
    }
    startupColor.Green += val;
  } else {
    if(startupColor.Green - val <= 0){
      startupColor.GreenDir *= -1;
      startupColor.Green += val;
    }
    startupColor.Green -= val;
  }
  if(startupColor.BlueDir == 1)
  {
    if(startupColor.Blue + val >= 255){
      startupColor.BlueDir *= -1;
      startupColor.Blue -= val;
    }
    startupColor.Blue += val;
  } else {
    if(startupColor.Blue - val <= 0){
      startupColor.BlueDir *= -1;
      startupColor.Blue += val;
    }
    startupColor.Blue -= val;
  }
}

function errorLeds(){
  var val = 1;
  if(errorBrightnessDir == 1){
    if(errorBrightness >= 12){
      errorBrightnessDir *= -1;
      errorBrightness -= val;
    } else {
      errorBrightness += val;
    }
  } else {
    if(errorBrightness <= 0){
      errorBrightnessDir *= -1;
      errorBrightness += val;
    } else {
      errorBrightness -= val;
    }
  }

  for(var x = 0; x <= LEDS; x++){
    LedDriver.setLedColor(x, errorBrightness, 255, 0, 0);
  }
  LedDriver.sendLeds();
}

app.get('/', (req, res) => {
    //with database => get data first
    res.sendFile(path.join(__dirname, 'public' ,"home.html"));
    //updateData();
});

app.get('/getData/:range', (req, res) => {
  
  var dateNow = new Date();
  var weekNr = moment().week();
  var data = null;
  var newData = {};
  try {
    var rawData = fs.readFileSync(path.join(__dirname, `data/last7Days.json`));
  } catch(err) {
    rawData = "{}";
  }
  data = JSON.parse(rawData);

  for(var element in data) {
    var day = new Date(element);
    // make a new array without objects older then the range (Hours)
    if(dateNow.getTime() - day.getTime() < parseInt(req.params.range) * 60 *60 * 1000) {
      newData[element] = data[element];
    }
    day = null;
  };
  res.send(newData);
  
})

http.listen(PORT, () => {
    console.log(`${new Date().toUTCString()}; Your server is running on port ${PORT}`);
    for(var x = 0; x <= LEDS; x++){
      LedDriver.setLedColor(x, 10, 0, 0, 0);
    }
    LedDriver.sendLeds();
});
