const i2c = require('i2c-bus');

class HIH6030 {
    constructor() {
        this.buffer = Buffer.alloc(4);
        this.address = 0x27;
        console.log(`${new Date().toUTCString()}; HIH6030 helper created!`)
    };

    log() {

        //new data in buffer
	this.write();
        setTimeout(() => {
	    this.read();
	    console.log(`HIH6030 - Humidity: ${this.getHum()}`);
	    console.log(`HIH6030 - Temperatuur: ${this.getTemp()}`);
	    console.log(`HIH6030 - Status: ${this.getStatus()}`);
	}, 200);
    };

    getData(_callback) {
      this.write();
      setTimeout(() => {
	        this.read();
      }, 200);
      _callback();
    };

    write() {
        // om een read-request te doen, moet er een 'write-bit' worden verzonden, 
        // omdat dit niet mogelijk is zonder een byte mee te verzenden,
        // schrijven we op een locatie dat we niet nodig hebben.
        // doen we dit niet, wordt er nooit opnieuw waarden ingelezen
        const i2cHIHWrite = i2c.openSync(1);
        i2cHIHWrite.writeByteSync(this.address, 0x10, 0x00);
        i2cHIHWrite.closeSync();
    }

    read() {
        //Na de write moeten we even wachten voordat we de nieuwe waarden kunnen inlezen
        const i2cHIHRead = i2c.openSync(1);
        i2cHIHRead.i2cReadSync(this.address, this.buffer.length, this.buffer);
        i2cHIHRead.closeSync();
    }

    getBuffer() {
        return this.buffer;
    };

    getAddress() {
        return this.address;
    };

    getStatus() {
        let status = this.buffer.slice(0, 2).readUInt16BE() & 0xc000;
        return parseInt(status >> 13);
    };

    getHum() {
        let value = this.buffer.slice(0, 2).readUInt16BE();
        let humidity = (value & 0x3fff) / (Math.pow(2, 14) - 2) * 100;
        return Math.round(humidity * 10) / 10;
    };

    getTemp() {
        let value = this.buffer.slice(2, 4).readUInt16BE();
        let celsius = (value >> 2) / (Math.pow(2, 14) - 2) * 165 - 40;
        return Math.round(celsius * 100) / 100;
    };
}

exports.default = new HIH6030()
