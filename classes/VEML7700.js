const i2c = require('i2c-bus');

class VEML7700 {
    constructor() {
        this.address = 0x10;
        console.log(`${new Date().toUTCString()}; VEML7000 helper created!`);
        this.light = 0;
    };


    startSensor() {
        try {
            const i2cVEML = i2c.openSync(1);
            i2cVEML.writeWordSync(this.getAddress(), 0x00, 0x0000);
            i2cVEML.closeSync();
        } catch {
        }
    }

    read() {
        //read command code 0x04 and turn sensor off
        try {
            const i2cVEML = i2c.openSync(1);
            this.light = i2cVEML.readWordSync(this.getAddress(), 0x04);
            i2cVEML.writeWordSync(this.getAddress(), 0x00, 0x0001);
            i2cVEML.closeSync();
        } catch {
            this.light = 1000;
        }
    };

    getAddress() {
        return this.address;
    };

    getLight() {
        return this.light;
    };
}

exports.default = new VEML7700()
