const i2c = require('i2c-bus');

class iAQCore {
    constructor() {
        this.buffer = Buffer.alloc(9);
        this.address = 0x5a;
        console.log(`${new Date().toUTCString()}; iAQCore helper created!`);
    };

    log() {

        //new data in buffer
        this.read();

        //process & log data
        console.log(`iAQ-Core - CO2 - ${this.getCO2()}`);
        console.log(`iAQ-Core - Resistance - ${this.getResistance()}`);
        console.log(`iAQ-Core - TVOC - ${this.getTVOC()}`);
        console.log(`iAQ-Core - Status - ${this.getStatus()}`);
    };

    read() {
        const i2cIAQ = i2c.openSync(1);
        i2cIAQ.i2cReadSync(this.getAddress(), this.getBuffer().length, this.getBuffer());
        i2cIAQ.closeSync();
    };

    getBuffer() {
        return this.buffer;
    };

    getAddress() {
        return this.address;
    };

    getStatus() {
        return parseInt(this.buffer.slice(2, 4).readUInt16BE() & 0xff00);
    };

    getCO2() {
        var value = this.buffer.slice(0, 2).readUInt16BE();
        return (value <= 2000 ? value : 2000);
    };

    getResistance() {
        return (this.buffer.slice(4, 8).readUInt32BE() & 0xffffff00) >> 8;
    };

    getTVOC() {
        return this.buffer.slice(7, 9).readUInt16BE();
    };
}

exports.default = new iAQCore()
