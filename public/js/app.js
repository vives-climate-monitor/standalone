
var socket = io();
socket.on('co2 messure', function(msg){
    document.getElementById("co2-tabel").innerText = msg + " ppm";
});
socket.on('temp messure', function(msg){
    document.getElementById("temp-tabel").innerText = msg + " °C";
});
socket.on('hum messure', function(msg){
    document.getElementById("hum-tabel").innerText = msg + " %";
});
socket.on('time messure', function(msg){
    var date = new Date(msg);
    document.getElementById("time-tabel").innerText = date.toLocaleString();
});


let myLineChart;  
let myBarChart;

var labelArray = [];
var dataArrayCo2 = [];
var dataArrayTemp = [];
var dataArrayHum = [];

var labelArrayBar = [];
var dataArrayCo2Bar = [];
var dataArrayTempBar = [];
var dataArrayHumBar = [];

var co2Minutes = 0;
var tempMinutes = 0;
var humMinutes = 0;

var co2Color = '#696969';
var tempColor = '#FF3860';
var humColor = '#00b3fa';

var barIsRenderd = false;

var refreshTimeout = setInterval(async() => {
    rangeLineChartChanged();
    LimitBarChartChanged();
  }, 60 * 1000);

function renderLineChart(id, labels, data, color, label) {
    const ctx = document.getElementById(id).getContext('2d');
    myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: labels,
        datasets: [{
            label: label,
            data: data,
            backgroundColor: [color]
        }]
    },
    options: {
        scales: {
        yAxes: [{
            ticks: {
            beginAtZero: true
            }
        }]
        }
    }
    });
}

function renderBarChart() {
    const ctx = document.getElementById('barChart').getContext('2d');
    myBarChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Minutes"],
        datasets: [{
            label: ["CO2"],
            backgroundColor: [co2Color],
            data: [co2Minutes]
        }, {
            label: ["Temperature"],
            backgroundColor: [tempColor],
            data: [tempMinutes]
        }, {
            label: ["Humidity"],
            backgroundColor: [humColor],
            data: [humMinutes]
        }]
    },
    options: {
        scales: {
        yAxes: [{
            ticks: {
            beginAtZero: true
            }
        }]
        }
    }
    });
}

async function updateBarChart() {
    myBarChart.data.datasets[0].data = [co2Minutes];
    myBarChart.data.datasets[1].data = [tempMinutes];
    myBarChart.data.datasets[2].data = [humMinutes];
    myBarChart.update(500);
}

async function updateLineChart(labels, data, label, backgroundColor) {
    myLineChart.data.labels = labels;
    myLineChart.data.datasets[0].data = data;
    myLineChart.data.datasets[0].label = label;
    myLineChart.data.datasets[0].backgroundColor = backgroundColor;
    myLineChart.update(0);
}

function getDataForRender(range) {
    axios.get(`/getData/${range}`).then(res => {

        for(var element in res.data) {
            labelArray.push(new Date(element).toLocaleTimeString());
            dataArrayCo2.push(res.data[element].co2);
            dataArrayTemp.push(res.data[element].temp);
            dataArrayHum.push(res.data[element].hum);
        }
        renderLineChart('lineChart', labelArray, dataArrayCo2, '#696969', 'CO2');
    })
}

async function getDataForBar() {
    return new Promise(async resolve => { 
        await axios.get(`/getData/24`).then(res => {
            
            labelArrayBar = [];
            dataArrayCo2Bar = [];
            dataArrayTempBar = [];
            dataArrayHumBar = [];

            var date;
            for(var element in res.data) {
                date = new Date(element);
                if(parseInt(date.getMinutes()) % 5 == 0) {
                    labelArrayBar.push(new Date(element).toLocaleTimeString());
                    dataArrayCo2Bar.push(res.data[element].co2);
                    dataArrayTempBar.push(res.data[element].temp);
                    dataArrayHumBar.push(res.data[element].hum);
                }
                date = null;
                    
            }

            resolve('resolved');
        })
    })
}

async function getDataForUpdate(range) {

    return new Promise(async resolve => { 
        await axios.get(`/getData/${range}`).then(res => {
            
            labelArray = [];
            dataArrayCo2 = [];
            dataArrayTemp = [];
            dataArrayHum = [];
            var date;

            if(range > 24) {
                for(var element in res.data) {
                    date = new Date(element);
                    if(range == 168) {
                        // range = 1 week => every 8 hours
                        if(date.getMinutes() == 0 && date.getHours() % 8 == 0) {
                            labelArray.push(new Date(element).toLocaleString());
                            dataArrayCo2.push(res.data[element].co2);
                            dataArrayTemp.push(res.data[element].temp);
                            dataArrayHum.push(res.data[element].hum);
                        }
                    }
                    date = null;
                }
            } else {
                for(var element in res.data) {
                    date = new Date(element);
                    if(range == 1) {
                        // range = 1 hour => every 5 minutes
                        if(parseInt(date.getMinutes()) % 5 == 0) {
                            labelArray.push(new Date(element).toLocaleTimeString());
                            dataArrayCo2.push(res.data[element].co2);
                            dataArrayTemp.push(res.data[element].temp);
                            dataArrayHum.push(res.data[element].hum);
                        }
                        date = null;
                    } else if(range == 3) {
                        // range = 3 hours => every 15 minutes
                        if(date.getMinutes() % 15 == 0) {
                            labelArray.push(new Date(element).toLocaleTimeString());
                            dataArrayCo2.push(res.data[element].co2);
                            dataArrayTemp.push(res.data[element].temp);
                            dataArrayHum.push(res.data[element].hum);
                        }
                        date = null;
                    } else if(range == 12) {
                       // range = 12 hours => every 30 minutes
                        if(date.getMinutes() % 30 == 0) {
                            labelArray.push(new Date(element).toLocaleTimeString());
                            dataArrayCo2.push(res.data[element].co2);
                            dataArrayTemp.push(res.data[element].temp);
                            dataArrayHum.push(res.data[element].hum);
                        }
                        date = null;
                    } else if(range == 24) {
                        // range = 1 day => every hour
                        if(date.getMinutes() == 0) {
                            labelArray.push(new Date(element).toLocaleTimeString());
                            dataArrayCo2.push(res.data[element].co2);
                            dataArrayTemp.push(res.data[element].temp);
                            dataArrayHum.push(res.data[element].hum);
                        }
                        date = null;
                    } else {
                    }
                    
                }
            }

            resolve('resolved');
        })
    })
}


function typeLineChartChanged() {
    var dataArrayByType= [];
    var colorByType = '';
    var labelByType = '';
    switch(document.getElementById('selType').value){
    case 'CO2': 
        dataArrayByType = dataArrayCo2;
        colorByType = co2Color;
        labelByType = 'CO2'
        break;
    case 'Temperature':
        dataArrayByType = dataArrayTemp;
        colorByType = tempColor;
        labelByType = 'Temperature'
        break;
    case 'Humidity':
        dataArrayByType = dataArrayHum;
        colorByType = humColor;
        labelByType = 'Humidity'
        break;
    default:
        dataArrayByType = dataArrayCo2;
        colorByType = co2Color;
    }
    updateLineChart(labelArray, dataArrayByType, labelByType, colorByType);
}


async function rangeLineChartChanged() {
    switch(document.getElementById('selRange').value){
    case '1 Hour': 
        await getDataForUpdate(1);
        break;
    case '3 Hours': 
        await getDataForUpdate(3);
        break;
    case '12 Hours': 
        await getDataForUpdate(12);
        break;
    case '1 Day': 
        await getDataForUpdate(24);
        break;
    case '1 Week': 
        await getDataForUpdate(7 * 24);
        break;
    default:
        await getDataForUpdate(1);
    }
    typeLineChartChanged();
}

async function LimitBarChartChanged() {
    await getDataForBar();
    co2Minutes = 0;
    tempMinutes = 0;
    humMinutes = 0;
    for(var element in dataArrayCo2Bar){
        if(dataArrayCo2Bar[element] > document.getElementById('selCo2').value){
            co2Minutes += 5;
        }
    }
    for(var element in dataArrayTempBar){
        if(dataArrayTempBar[element] > document.getElementById('selTemp').value){
            tempMinutes += 5;
        }
    }
    for(var element in dataArrayHumBar){
        if(dataArrayHumBar[element] > document.getElementById('selHum').value){
            humMinutes += 5;
        }
    }
    if(barIsRenderd) {
        // update chart
        updateBarChart();
    } else {
        // render chart
        renderBarChart();
        barIsRenderd = true;
    }
}


getDataForRender(1);
LimitBarChartChanged();